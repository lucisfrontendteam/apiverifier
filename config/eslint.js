const Levels = {
  OFF: 0,
  WARNING: 1,
  ERROR: 2
};

// 사용 가능한 rules
// https://www.npmjs.com/package/eslint-plugin-react
module.exports = {
  extends: "airbnb",
  rules: {
    "react/prefer-stateless-function": Levels.OFF,
  }
};

