import React, { PureComponent } from 'react';
import { version, name as programName } from '../../package.json';
import { connect } from 'react-redux';
import { actions } from '../Redux';
// eslint-disable-next-line
import $ from 'jquery';
import 'bootstrap';
import '../Resources/css/jquery-ui.css';
import '../Resources/css/jquery.timepicker.css';
import '../Resources/css/jquery.dataTables.css';
import '../Resources/css/jquery.mCustomScrollbar.css';
import '../Resources/css/jqtree.css';
import '../Resources/css/bootstrap.css';
import '../Resources/css/bootstrap-datepicker.standalone.css';
import '../Resources/css/lucis-base.css';
import '../Resources/css/lucis-custom.css';
import '../Resources/css/lucis-widget.css';
import '../Resources/css/lucis-theme.css';
import AdvencedFilter from '../../../userdashboard/src/Components/AdvencedFilter';
import QueryViewer from '../Components/QueryViewer';
import ApiViewer from '../Components/ApiViewer';
import AjaxResultViewer from '../Components/AjaxResultViewer';
import SampleJsonViewer from '../Components/SampleJsonViewer';

class MainPage extends PureComponent {
  componentDidMount() {
    const { props } = this;
    const NetWorkModeState = { label: '오프라인 모드', value: false };
    const newProps = {
      ...props,
      NetWorkModeState,
    };
    props.advencedfilterargmanagerreadInitAction(newProps);
    props.querystatemanagerreadInitAction(newProps);
  }

  render() {
    const { props } = this;
    return (
      <div>
        <header className="main-header">
          <a href="#" className="logo">
            <span className="logo-lg">{programName}</span>
          </a>
        </header>
        <section className="content-inner">
          <div className="row">
            <div className="col-md-12">
              <AdvencedFilter {...props} isTestMode="true" />
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <p>API 리스트</p>
              <ApiViewer {...props} />
            </div>
            <div className="col-md-6">
              <p>파라미터</p>
              <QueryViewer {...props} />
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <p>호출 후의 결과값</p>
              <AjaxResultViewer {...props} />
            </div>
            <div className="col-md-6">
              <p>샘플</p>
              <SampleJsonViewer {...props} />
            </div>
          </div>
        </section>
        <footer className="main-footer">
          <div className="pull-right hidden-xs">
            버젼: {version}
          </div>
          <strong>Copyright &copy; 2016 <a href="#">LUCIS</a>.</strong> All rights reserved.
        </footer>
        <div className="control-sidebar-bg"></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
export default connect(mapStateToProps, actions)(MainPage);
