import React, { PureComponent } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { actions } from '../Redux';
import UIContentsLoader from '../../../userdashboard/src/Utils/UIContentsLoader';
const {
  WidgetCores,
  WidgetRefiner,
  CommonManagerSampleDatas,
} = UIContentsLoader;
const { widgetSampleDatas } = new WidgetRefiner(WidgetCores);
import DashBoardLoader from '../../../userdashboard/src/Utils/DashBoardLoader';
const { componentSampleDatas } = new DashBoardLoader();

class SampleJsonViewer extends PureComponent {
  render() {
    const { props } = this;
    return (
      <textarea
        className="col-md-12"
        id="resultViewer"
        rows="60"
        readOnly="true"
        value={props.deserializedJson}
      />
    );
  }
}

const mapStateToProps = (state) => {
  const targetManifest = _.find(state.APIListState, manifest => manifest.selected);
  const targetSampleName = `${targetManifest.id}SampleDatas`;
  const allSampleDatas = {
    ...widgetSampleDatas,
    ...componentSampleDatas,
    ...CommonManagerSampleDatas,
  };
  const targetState = allSampleDatas[targetSampleName];
  const deserializedJson = JSON.stringify(targetState, null, 2);
  return {
    ...state,
    deserializedJson,
  };
};
export default connect(mapStateToProps, actions)(SampleJsonViewer);
