import React, { PureComponent } from 'react';

export default class QueryViewer extends PureComponent {
  render() {
    const { props } = this;
    const value = JSON.stringify(props.QueryStateManagerState, null, 2);

    return (
      <textarea
        className="col-md-12" value={value}
        id="queryViewer" rows="60" readOnly="true"
      />
    );
  }
}
