import React, { PureComponent } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { actions } from '../Redux';

class AjaxResultViewer extends PureComponent {
  render() {
    const { props } = this;
    return (
      <textarea
        className="col-md-12"
        id="resultViewer"
        rows="60"
        readOnly="true"
        value={props.deserializedJson}
      />
    );
  }
}

const mapStateToProps = (state) => {
  const targetManifest = _.find(state.APIListState, manifest => manifest.selected);
  const targetStateName = `${targetManifest.stateId}State`;
  const targetState = state[targetStateName];
  const deserializedJson = JSON.stringify(targetState, null, 2);
  return {
    ...state,
    deserializedJson,
  };
};
export default connect(mapStateToProps, actions)(AjaxResultViewer);
