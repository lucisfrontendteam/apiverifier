import React, { PureComponent } from 'react';
import _ from 'lodash';
import { v4 } from 'uuid';

export default class ApiViewer extends PureComponent {
  handleSelectApi(param) {
    const { id, apiUrl } = param;
    const { props } = this;
    props.apiSelectAction(id);
    const lowerId = _.lowerCase(id);
    const actionName = `${lowerId}InitAction`.replace(/ /g, '');
    const newProps = {
      ...props,
      apiUrl,
    };
    props[actionName](newProps);
  }

  getFormProps(apiUrl, inputIdList) {
    const { props } = this;
    const newProps = {
      ...props,
      apiUrl,
      QueryIdMap: {},
    };
    const formData = new FormData();
    _.each(inputIdList, info => {
      const { id, key, type } = info;
      const { value, files } = document.getElementById(id);
      if (type === 'file') {
        formData.append(key, files[0]);
      } else {
        formData.append(key, value);
      }
    });
    newProps.QueryIdMap = formData;
    return newProps;
  }

  getIdListProps(apiUrl, inputIdList) {
    const { props } = this;
    const newProps = {
      ...props,
      apiUrl,
      QueryIdMap: {},
    };
    _.each(inputIdList, info => {
      const { id, key, json } = info;
      if (json) {
        newProps.QueryIdMap[key] = JSON.parse(document.getElementById(id).value);
      } else {
        newProps.QueryIdMap[key] = document.getElementById(id).value;
      }
    });
    return newProps;
  }

  getLoginIdProps(apiUrl, idElementInfo) {
    const { value: userId = '' }
      = document.getElementById(idElementInfo.id);
    const { props } = this;
    const newProps = {
      ...props,
      LoginManagerState: {
        userId,
      },
      apiUrl,
    };
    return newProps;
  }

  getLoginIdPassProps(
    apiUrl,
    idElementInfo = { id: '' },
    passElementInfo = { id: '' }
  ) {
    const { value: userId }
      = document.getElementById(idElementInfo.id) || { value: '' };
    const { value: userPass }
      = document.getElementById(passElementInfo.id) || { value: '' };
    const { props } = this;
    const newProps = {
      ...props,
      LoginManagerState: {
        userId,
        userPass,
      },
      apiUrl,
    };
    return newProps;
  }

  handleLoginApi(param) {
    const { id, apiUrl, inputIdList, type } = param;
    const { props } = this;
    props.apiSelectAction(id);
    const lowerId = _.lowerCase(id);
    const actionName = `${lowerId}InitAction`.replace(/ /g, '');
    const [idElementInfo, passElementInfo] = inputIdList;
    let newProps = {};
    switch (type) {
      case 'LoginId': {
        newProps = this.getLoginIdProps(apiUrl, idElementInfo);
      } break;

      case 'IdList' : {
        newProps = this.getIdListProps(apiUrl, inputIdList);
      } break;

      case 'multipart' : {
        newProps = this.getFormProps(apiUrl, inputIdList);
      } break;

      default: {
        newProps = this.getLoginIdPassProps(apiUrl, idElementInfo, passElementInfo);
      }
    }
    props[actionName](newProps);
  }

  renderNormalRecord(param) {
    const {
      className,
      apiUrl,
      style,
      name,
      usage,
      method,
    } = param;
    return (
      <li
        key={v4()}
        className={className}
        onClick={() => this.handleSelectApi(param)}
        style={style}
      >
        {this.renderMethodLabel(method)}
        <span>[{name}][{usage}] {apiUrl}</span>
      </li>
    );
  }

  renderInput(el) {
    return (
      <span
        key={v4()}
        style={{
          marginLeft: '10px',
          marginTop: '10px',
        }}
      >
        <input
          type="text"
          className="form-control"
          id={el.id}
          placeholder={el.key}
          style={{ width: '170px' }}
        />
      </span>
    );
  }

  renderFileInput(el) {
    const { id, key } = el;
    return (
      <span
        key={v4()}
        style={{
          marginLeft: '10px',
          marginTop: '10px',
        }}
      >
        <label htmlFor={id}>{key}</label>
        <input
          id={id}
          type="file"
          placeholder={key}
          style={{ width: '170px' }}
          className="form-control"
        />
      </span>
    );
  }

  renderTextArea(el) {
    const { id, json, key, rows } = el;
    const deserializedValue = JSON.stringify(json, null, 2);
    return (
      <span
        key={v4()}
        style={{
          marginLeft: '10px',
          marginTop: '10px',
        }}
      >
        <label htmlFor={id}>{key}</label>
        <textarea
          type="text"
          className="form-control"
          id={id}
          placeholder={key}
          style={{ width: '170px' }}
          defaultValue={deserializedValue}
          rows={rows}
        />
      </span>
    );
  }

  renderInputs(inputIdList) {
    return _.map(inputIdList, el => {
      let targetFromElement = {};
      if (el.json) {
        targetFromElement = this.renderTextArea(el);
      } else if (el.type === 'file') {
        targetFromElement = this.renderFileInput(el);
      } else {
        targetFromElement = this.renderInput(el);
      }
      return targetFromElement;
    });
  }

  renderLoginRecord(param) {
    const {
      className,
      apiUrl,
      style,
      name,
      inputIdList,
      method,
      usage,
    } = param;
    return (
      <li
        key={v4()}
        className={className}
        style={style}
      >
        {this.renderMethodLabel(method)}
        <span>[{name}][{usage}] {apiUrl}</span>
        {this.renderInputs(inputIdList)}
        <span style={{ paddingLeft: '10px' }}>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={() => this.handleLoginApi(param)}
          >요청</button>
        </span>
      </li>
    );
  }

  renderIdListRecord(param) {
    const {
      className,
      apiUrl,
      style,
      name,
      inputIdList,
      method,
      usage,
    } = param;
    return (
      <li
        key={v4()}
        className={className}
        style={style}
      >
        {this.renderMethodLabel(method)}
        <span>[{name}][{usage}] {apiUrl}</span>
        {this.renderInputs(inputIdList)}
        <span style={{ paddingLeft: '10px' }}>
          <button
            type="submit"
            className="btn btn-primary"
            onClick={() => this.handleLoginApi(param)}
          >요청</button>
        </span>
      </li>
    );
  }

  renderMethodLabel(method) {
    let targetLabel = '';
    switch (method) {
      case 'post': {
        targetLabel = (<span className="label label-warning">POST</span>);
      } break;

      case 'put': {
        targetLabel = (<span className="label label-success">PUT</span>);
      } break;

      case 'delete': {
        targetLabel = (<span className="label label-danger">DELETE</span>);
      } break;

      case 'patch': {
        targetLabel = (<span className="label label-info">PATCH</span>);
      } break;

      default:
      case 'get': {
        targetLabel = (<span className="label label-default">GET</span>);
      }
    }
    return targetLabel;
  }

  renderRecords() {
    const { props } = this;
    return _.map(props.APIListState, manifest => {
      const { selected, apiUrl, id, name, type, inputIdList, method, usage } = manifest;
      const active = selected ? 'active' : '';
      const className = `list-group-item ${active}`;
      const style = { cursor: 'pointer' };
      const param = {
        id,
        className,
        apiUrl,
        style,
        name,
        inputIdList,
        type,
        method,
        usage,
      };
      let targetRecord = {};
      switch (type) {
        case 'LoginId':
        case 'LoginIdPass': {
          targetRecord = this.renderLoginRecord(param);
        } break;

        case 'multipart':
        case 'IdList': {
          targetRecord = this.renderIdListRecord(param);
        } break;

        default: {
          targetRecord = this.renderNormalRecord(param);
        }
      }
      return targetRecord;
    });
  }

  render() {
    const style = {
      height: '700px',
      overflowY: 'scroll',
    };
    return (
      <ul className="list-group" style={style}>
        {this.renderRecords()}
      </ul>
    );
  }
}
