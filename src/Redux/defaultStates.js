import _ from 'lodash';
import UIContentsLoader from '../../../userdashboard/src/Utils/UIContentsLoader';
const {
  WidgetCores,
  WidgetRefiner,
  CommonManagerManifest,
} = UIContentsLoader;
import DashBoardLoader from '../../../userdashboard/src/Utils/DashBoardLoader';

export default class DefaultStates {
  get apiListState() {
    const { widgetManifest } = new WidgetRefiner(WidgetCores);
    const { componentManifest } = new DashBoardLoader();
    const commonManagerManifestList
      = _.map(CommonManagerManifest, manifest => manifest);
    const apiList = [
      ...widgetManifest,
      ...componentManifest,
      ...commonManagerManifestList,
    ];
    apiList[0].selected = true;
    return apiList;
  }
}
