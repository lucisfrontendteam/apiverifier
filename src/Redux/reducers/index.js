import { combineReducers } from 'redux';
import { dashboardReducers } from '../../../../userdashboard/src/Redux/reducers';
import UIContentsLoader from '../../../../userdashboard/src/Utils/UIContentsLoader';
const {
  CommonManagerReducers,
} = UIContentsLoader;
import APIListState from './APIListReducer';
export default combineReducers({
  ...dashboardReducers,
  ...CommonManagerReducers,
  APIListState,
});
