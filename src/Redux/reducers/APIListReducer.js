import * as actionTypes from '../actionTypes';
import _ from 'lodash';

export default function APIListState(state = [], action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.API_LIST_INIT: {
      const { apiListState } = action.payload;
      finalState = [...apiListState];
    } break;

    case actionTypes.API_SELECT: {
      const { id } = action.payload;
      finalState = _.map(state, manifest => {
        const newState = {
          selected: false,
          ...manifest,
        };
        newState.selected = manifest.id === id;
        return newState;
      });
    } break;

    default: {
      finalState = [...state];
    }
  }

  return finalState;
}
