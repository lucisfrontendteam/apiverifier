import * as apiVerifierActions from './actionTypes';
import { actions as dashboardActions } from '../../../userdashboard/src/Redux';
import UIContents from '../../../userdashboard/src/Utils/UIContentsLoader';
const {
  LoginManagerActions,
  ProjectManagerActions,
  CommonManagerActionList,
} = UIContents;
import DefaultStates from './defaultStates';
const defaultStates = new DefaultStates();

function apiListInitAction(apiListState = defaultStates.apiListState) {
  return {
    type: apiVerifierActions.API_LIST_INIT,
    payload: { apiListState },
  };
}

function apiSelectAction(id = '') {
  return {
    type: apiVerifierActions.API_SELECT,
    payload: { id },
  };
}

export const actions = {
  ...dashboardActions,
  ...LoginManagerActions,
  ...ProjectManagerActions,
  ...CommonManagerActionList,
  apiListInitAction,
  apiSelectAction,
};
